# Al correr el servidor de ngixn y php-fpm

```sh
docker-compose up
```
Visitar [Localhost](http://php-fpm.localhost:8080/)

Ir a `localhost:8080` en tu navegador favorito. En la url agregar `php-fpm.` a `localhost:8080`.

La url deber quedar de la siguiente forma:

```sh
http://php-fpm.localhost:8080/
```

listo, tienes corriendo tu primer servidor `nginx` con `docker-compose`, en el puerto `8080` y `php` en su contendor.

Mas detalles de configuracion de `docker-compose` y `php`. [Dockerice su aplicación PHP con Nginx y PHP8-FPM](https://marcit.eu/en/2021/04/28/dockerize-webserver-nginx-php8/)

  >   Autor: Esta guía y configuración nunca deben usarse en un servidor de producción y no es un ejemplo de las mejores prácticas. Esta publicación está pensada como una introducción a `Docker Compose` y `php` para principiantes.

  # Nunca pares de Aprender.

