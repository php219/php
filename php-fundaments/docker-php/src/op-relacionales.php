<?php

$a = 5;
$b = 5;
$b1 = "5";
$c = 9;
$d = 2;

// == igual
/* var_dump( $a == $b );
echo "\n"; */

// === identico
/* var_dump( $a === $b1 );
echo "\n"; */

// != diferente
/* var_dump( $a != $b);
echo "\n";
var_dump( $a != $b1);
echo "\n";
 */

//  !== Diferente
/* var_dump($a !== $b);
echo "\n";
var_dump($a !== $b1);
echo "\n"; */

// < menor que
/* var_dump( $a < $b );
var_dump( $c < $b );
var_dump( $d < $b );
 */
// > Mayor que
/* var_dump( $a > $b );
var_dump( $c > $b );
var_dump( $d > $b ); */

// >= Mayor o igual que

/* var_dump( $a >= $b );
var_dump( $c >= $b );
var_dump( $d >= $b ); */

// <= Menor o igual que

/* var_dump( $a <= $b );
var_dump( $c <= $b );
var_dump( $d <= $b ); */

// <=> Nave espacial

// echo 2 <=> 1; // 1
// echo 1 <=> 1; // 0
// echo -50 <=> 1; // -1

// ?? Fusión de null

$edad_de_pepito = 23;

echo $edad_de_juanito ?? $edad_de_pepito ?? $edad_de_jaimito;

echo "\n";